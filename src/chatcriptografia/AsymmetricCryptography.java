//https://www.mkyong.com/java/java-asymmetric-cryptograph
package chatcriptografia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

//import org.apache.commons.codec.binary.Base64;
public class AsymmetricCryptography {

    Cipher cipher;

    public AsymmetricCryptography() {
        try {
            cipher = Cipher.getInstance("RSA");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AsymmetricCryptography.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(AsymmetricCryptography.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public byte[] encryptPrivate(byte[] data, PrivateKey key)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            UnsupportedEncodingException, IllegalBlockSizeException,
            BadPaddingException, InvalidKeyException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        // String cadena = Base64.getEncoder().encodeToString(cipher.doFinal(msg.getBytes("UTF-8")));
        byte[] encrypted = cipher.doFinal(data);
        return encrypted;
    }

    public byte[] decryptPublic(byte[] data, PublicKey key)
            throws InvalidKeyException, UnsupportedEncodingException,
            IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException {

        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(data);
    }

        public byte[] encryptPublic(byte[] data,PublicKey  key)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            UnsupportedEncodingException, IllegalBlockSizeException,
            BadPaddingException, InvalidKeyException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        // String cadena = Base64.getEncoder().encodeToString(cipher.doFinal(msg.getBytes("UTF-8")));
        byte[] encrypted = cipher.doFinal(data);
        return encrypted;
    }

    public byte[] decryptPrivate(byte[] data,PrivateKey  key)
            throws InvalidKeyException, UnsupportedEncodingException,
            IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException {

        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(data);
    }

}
