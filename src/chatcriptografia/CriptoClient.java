/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatcriptografia;

import com.sun.xml.internal.ws.client.sei.ValueSetterFactory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author alex
 */
public class CriptoClient {

    GenerateKeys clientKeys;
    PublicKey serverKey;
    AsymmetricCryptography Asym;
    String serverAddress = "localhost";
    InputStream in;
    OutputStream out;

    public static void main(String[] args) throws InterruptedException {
        try {
            CriptoClient client = new CriptoClient();
            client.connectToServer();
            client.handShake();
            Thread.sleep(500);
            client.communication();
        } catch (IOException ex) {
            Logger.getLogger(CriptoClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Constructs the client by laying out the GUI and registering a listener
     * with the textfield so that pressing Enter in the listener sends the
     * textfield contents to the server.
     */
    public CriptoClient() {
        CriptoServer cs = new CriptoServer();

        new Thread(() -> {
            cs.init();
        }).start();// LLENÇO EL SERVER EN UN THREAD 

    }

    /**
     * Implements the connection logic by prompting the end user for the
     * server's IP address, connecting, setting up streams, and consuming the
     * welcome messages from the server. The Capitalizer protocol says that the
     * server sends three lines of text to the client immediately after
     * establishing a connection.
     */
    public void connectToServer() throws IOException {
        //// GENERA KEYS PUBLIC / PRIVATE. GUARDALES

        // CONNEXIO
        System.out.println("Welcome to the Capitalization Program");
        System.out.println("Client Connecting to server...");

        Socket socket = new Socket(serverAddress, 9898);
        in = socket.getInputStream();
        out = socket.getOutputStream();

    }

    public void handShake() throws IOException {
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");        
        System.out.println("BEGIN CLIENT HANDSHAKE");
        byte[] clau = null;

        //// LLEGEIXO LA CLAU PUBLICA DEL SERVER.
        //// AMB ELS BYTES LLEGITS GENEREM LA CLAU PUBLICA: KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(clau));
        ////LA GUARDEM A serverKey
        System.out.println("Server Public Key Received: ");
        System.out.println(Arrays.toString(clau));
        System.out.println("Sending Client Public Key");
        System.out.println("END CLIENT HANDSHAKE");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
    }

    public void communication() {
        System.out.println("Comença la comunicació xifrada!!");
        AsymmetricCryptography asym = null;
        byte[] textEncriptatBytes = null;
        byte[] bytesDesencriptats = null;
        String textDesencriptat = null;
        String textClar = null;
        Boolean endCommunication = false;
        byte[] textBytes = null;

        while (!endCommunication) {

            //// INPUT USER LOWERCASE TEXT
            System.out.println("Enter lowercase text: ");
            Scanner scanner = new Scanner(System.in);
            textClar = scanner.nextLine();

            //// TODO ENCRIPT WITH SERVER PUBLIC KEY
            //// SEND TO SERVER
            ByteArrayCommunicator.sendBytes(textClar.getBytes(), out);

            //// LLEGEIX DEL SERVER EL LA RESPOSTA ENCRIPTADA
            textEncriptatBytes = ByteArrayCommunicator.readBytes(in);
            System.out.println("CLIENT REBEM RESPOSTA XIFRADA>>> " + Arrays.toString(textEncriptatBytes));
            //// DECRYPT WITH  CLIENT PRIVATE KEY
            bytesDesencriptats = /**TODO DESENCRIPTA AMB LA TEVA CLAU PRIVADA   */     textEncriptatBytes;

            textDesencriptat = new String(bytesDesencriptats);
            System.out.println("CLIENT: DESXIFREM LA RESPOSTA >>> " + textDesencriptat);
            if (textDesencriptat.equals("quit")) {
                endCommunication = true;
            }
        }

    }

}
