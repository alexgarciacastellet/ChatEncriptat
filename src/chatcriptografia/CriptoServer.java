/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatcriptografia;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author alex
 */
public class CriptoServer {

    private Socket socket;
    GenerateKeys serverKeys;
    PublicKey clientKey;
    AsymmetricCryptography asym;
    InputStream in;
    OutputStream out;

    public static void main(String[] args) throws Exception {
        CriptoServer server;
        server = new CriptoServer();
        server.initServer();
        server.handShake();
        server.communication();
    }

    
   public void init(){
        try {
            this.initServer();
            this.handShake();
            this.communication();
        } catch (IOException ex) {
            Logger.getLogger(CriptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CriptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(CriptoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }    
    
    /**
     * Services this thread's client by first sending the client a welcome
     * message then repeatedly reading strings and sending back the capitalized
     * version of the string.
     */
    public void initServer() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        System.out.println("The capitalization server is running.");
        // ENGEGUEM EL SERVIDOR
        ServerSocket listener = new ServerSocket(9898);
        socket = listener.accept();
        this.socket = socket;
        in = socket.getInputStream();
        out = socket.getOutputStream();
        System.out.println("New connection at " + socket);
    }

    public void handShake() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");    
        System.out.println("START SERVER HANDSHAKE");
        //// GENERA KEYS DE 1024 (serverKeys = new GenerateKeys ... )
        System.out.println("Sending Server Public Key");
        //// ENVIA PUBLIC KEY CAP EL CLIENT
        //// LLEGEIX LA RESPOSTA DEL CLIENT, EN byte[]
        //// HA DE SER LA SEVA CLAU PUBLICA, 
        //// GENEREM CLAU: KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(key));
        //// LA GUARDEM A clientKey

        System.out.println("END SERVER HANDSHAKE");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
    }
    
    public void communication(){
        AsymmetricCryptography asym = null;
        byte[] textEncriptatBytes = null;
        byte[] bytesDesencriptats = null;
        String textClar = "no text";
        Boolean endCommunication=false;
        byte[] textBytes = textClar.getBytes();
        
        ////INSTANCIO AsymmetricCryptography

        while(!endCommunication){
        //// LLEGIR DEL SOCKET
        textEncriptatBytes = ByteArrayCommunicator.readBytes(in);
        System.out.println("SERVER: REBO DADES XIFRADES>>> "+  Arrays.toString(textEncriptatBytes) );
        
        //// DESXIFRAR AMB LA MEVA CLAU PRIVADA
        bytesDesencriptats = /* PROCESSA ELS BYTES LLEGITS PER DESXIFRAR-LOS AMB  AsymmetricCryptography */ textEncriptatBytes;
        textClar = new String(bytesDesencriptats);
        
        //// MOSTRA PER PANTALLA EL TEXT CLAR
        System.out.println("SERVER: TEXT DESENCRIPTAT>>> "+  textClar );
        
        //// TO UPPERCASE
        textClar = textClar.toUpperCase();
        System.out.println("SERVER: CONVERTING TO UPPERCASE >>> "+  textClar );
        //// ENCRIPTA AMB CLAU PUBLICA DEL CLIENT
        textEncriptatBytes = /* ENCRIPTA AMB LA CLAU PUBLICA DEL CLIENT */ textClar.getBytes();
                
        //// ENVIA-LI
        ByteArrayCommunicator.sendBytes(textEncriptatBytes, out);
        if (textClar.equals("quit")) endCommunication= true;
        }            

    }

}
